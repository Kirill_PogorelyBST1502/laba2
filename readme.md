### Лабораторная работа № 2
---
## Задание: 
Даны 3 стержня и n-ое количество дисков на одном из них. Требуется перенести диски на другой стержень не утратив порядок
их расположения. Для решения проблемы создать класс hanoi с 3 методами:

* display() - отображает положение дисков на стержнях при каждом ходе.

* move() - перемещает диск с одного стержня на другой.

* solve() - решает проблему переноса дисков с одного стержня на другой.

Пользователь при вызове передаёт один аргумент - количество дисков. Если передается число не больше нуля, то программа выдаёт ошибку с просьбой ввести число больше нуля.

---
## Код программы:
```python
#!/usr/bin/python
# -*- coding: utf-8 -*-
import sys

class hanoi:

	#Класс,который реализует решение задачи Ханойской башни

	kernels = [0,1,2]
	list_kernels = [ [],[],[]]
	i = 1
	go_ = False
	def __init__(self,n): #Конструктор класса для обрабтоки входного аргумента
		try:
			self.kol = n
			if self.kol <= 0:
				raise ValueError
		except ValueError:
			print "Enter number biggest than 0"
		else:
			self.go_ = True
			while self.i <= n:
				self.list_kernels[0].append(self.i)
				self.i+=1


	def Display(self,step):  #Метод для отображения положения дисков на стержнях при каждом ходе
		print "\n"
		if step != 0:
			print "Step number" + str(step)
		else:
			print "default postiion of disks"
		print str(self.kernels[0]) + ":",",".join(map(str, self.list_kernels[0]))
		print str(self.kernels[1]) + ":",",".join(map(str, self.list_kernels[1]))
		print str(self.kernels[2]) + ":",",".join(map(str, self.list_kernels[2]))
		print "Peremeshaetsa disk : " + str(peremesh_disk) + "\t" + "Na kakoi sterjen" + str(sterj) 
		print "\n"


	def Move(self,number_kernel_add,number_kernel_del,number_disk): #Метод, которая перемещает диск с одного стержня на другой
		number_kernel_add.insert(0,number_disk)
		del number_kernel_del[0]


	def Solve(self): 
		'''
		Метод,решающая проблему переноса дисков с одного стержня на другой
		'''
		self.display(0)
		long_mass = len(self.list_kernels[0]) - 1
		self.i = 1
		while self.i <= long_mass :
			self.move(self.list_kernels[2],self.list_kernels[0],self.i)
			self.display(self.i)
			self.i+=1
		self.move(self.list_kernels[1],self.list_kernels[0],self.i)
		self.display(self.i)
		nextstep = self.i
		self.i-=1
		while self.i > 0:
			self.move(self.list_kernels[1],self.list_kernels[2],self.i)
			nextstep+=1
			self.display(nextstep)
			self.i-=1


watch = hanoi(int(sys.argv[1])) 
if watch.go_:
	watch.solve()

```
---
## Результат выполнения программы:
```
kirill@kirill-VirtualBox:~/Документы/laba2$ ./hanoiTWO 4


Исходное положение дисков
0: 1,2,3,4
1: 
2: 




Шаг номер 1
0: 2,3,4
1: 
2: 1




Шаг номер 2
0: 3,4
1: 
2: 2,1




Шаг номер 3
0: 4
1: 
2: 3,2,1




Шаг номер 4
0: 
1: 4
2: 3,2,1




Шаг номер 5
0: 
1: 3,4
2: 2,1




Шаг номер 6
0: 
1: 2,3,4
2: 1




Шаг номер 7
0: 
1: 1,2,3,4
2: 
```
---
## Вывод: 
Я ознакомился с основами ООП python'а.



